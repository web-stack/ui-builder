const MinifyPlugin = require("babel-minify-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;
const ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

const webpack = require("webpack");
const config = require("./merged.config");
const path = require("path");

function makePluginsConfig() {
  const env = require("../utils/env");
  const htmlEntries = Object.keys(config.entries).map((key) => {
    return new HtmlWebpackPlugin({
      filename: `${key}.html`,
      template:
        config.template || path.resolve(__dirname, "../public/index.html"),
      chunks: [key],
    });
  });

  const plugins = [
    ...htmlEntries,
    new CleanWebpackPlugin(),
    new ForkTsCheckerWebpackPlugin(),
  ];

  console.log("Production mode:", env.isProduction());
  if (env.isProduction()) {
    plugins.push(
      new webpack.DefinePlugin({
        "process.env.NODE_ENV": '"production"',
      })
    );
    plugins.push(new MinifyPlugin());
    plugins.push(
      new BundleAnalyzerPlugin({
        openAnalyzer: env.isProduction(),
        analyzerMode: "static",
        reportFilename: path.resolve(env.projectDir, "report/index.html"),
      })
    );
    plugins.push(
      new CopyWebpackPlugin({
        patterns: [
          {
            from: path.resolve(env.projectDir, config.assets),
            to: "",
            globOptions: {
              ignore: ["index.html"],
            },
          },
        ],
      })
    );
  }

  return plugins;
}

module.exports = {
  makePluginsConfig,
};
