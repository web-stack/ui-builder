const path = require("path");
const env = require("../utils/env");
const config = require("./merged.config");

function makeEntryConfig() {
  const entries = Object.entries(config.entries).reduce(
    (value, [name, entryPath]) => {
      const entry = [];
      if (!env.isProduction()) {
        entry.push("core-js/stable", "react-hot-loader/patch");
      }
      entry.push("regenerator-runtime/runtime");
      entry.push(path.resolve(env.projectDir, entryPath));

      return { ...value, [name]: entry };
    },
    {}
  );

  console.log("[ui-builder]", "generating entries: ");
  console.dir(entries);

  return entries;
}

module.exports = {
  makeEntryConfig,
};
