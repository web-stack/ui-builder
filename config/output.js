const env = require("../utils/env");
const path = require("path");
const config = require("./merged.config");

function makeOutputConfig() {
  return {
    path: path.resolve(env.projectDir, config.output),
    filename: env.isProduction() ? "[name].[hash:8].js" : "[name].js",
    sourceMapFilename: env.isProduction()
      ? "[name].[hash].source.map"
      : "[name].[contenthash].map",
    chunkFilename: env.isProduction()
      ? "[name].[id].[hash:8].js"
      : "[name].[contenthash].chunk.js",
    publicPath: "/",
  };
}

module.exports = {
  makeOutputConfig,
};
