const defaultConfig = require("./default.config");
const userConfig = require("../utils/userConfig");

module.exports = {
  ...defaultConfig,
  ...userConfig,
};
