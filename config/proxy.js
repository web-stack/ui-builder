const config = require("./merged.config");

module.exports = {
  getProxyMap: () => {
    return config.proxy;
  },
};
