const path = require("path");
const env = require("./utils/env");
const { makeEntryConfig } = require("./config/entry");
const { makeOutputConfig } = require("./config/output");
const { makePluginsConfig } = require("./config/plugins");
const { getProxyMap } = require("./config/proxy");
const { resolveWebpackAliases } = require("./utils/resolveAliases");
const config = require("./config/merged.config");

module.exports = {
  mode: env.isProduction() ? "production" : "development",
  entry: makeEntryConfig(),
  devtool: env.isProduction() ? "source-map" : "eval-source-map",
  devServer: {
    hot: true,
    open: true,
    https: config.secure,
    proxy: getProxyMap(),
  },
  output: makeOutputConfig(),
  resolve: {
    alias: {
      "react-dom": "@hot-loader/react-dom",
      ...resolveWebpackAliases(),
    },
    extensions: [".ts", ".tsx", ".js"],
    modules: [
      path.join(env.projectDir, "node_modules"),
      path.join(__dirname, "node_modules"),
      "node_modules",
    ],
  },
  optimization: {
    minimize: env.isProduction(),
    runtimeChunk: {
      name: "runtime",
    },
    splitChunks: {
      chunks: "all",
    },
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              "@babel/preset-env",
              "@babel/preset-typescript",
              "@babel/preset-react",
            ],
            plugins: [
              ["@babel/plugin-proposal-decorators", { legacy: true }],
              ["@babel/plugin-proposal-class-properties", { loose: true }],
              "react-hot-loader/babel",
            ],
          },
        },
      },
      {
        test: /\.css$/i,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              sourceMap: !env.isProduction(),
              modules: {
                localIdentName: "[name]__[local]___[hash:base64:5]",
              },
            },
          },
        ],
      },
      {
        test: /\.scss$/i,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              sourceMap: !env.isProduction(),
              modules: {
                localIdentName: "[name]__[local]___[hash:base64:5]",
              },
            },
          },
          "sass-loader",
        ],
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/,
        use: "file-loader",
      },
      {
        test: /\.svg$/,
        use: "@svgr/webpack",
      },
    ],
  },
  plugins: makePluginsConfig(),
};
