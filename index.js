#!/usr/bin/env node
const yargs = require("yargs");
const webpack = require("webpack");
const WebpackDevServer = require("webpack-dev-server");
const { run } = require("jest-cli");
const path = require("path");
const fs = require("fs");
const env = require("./utils/env");

const argv = yargs
  .command("init", "Initialize application")
  .command("build", "Create app bundle")
  .command("start", "Start a dev server")
  .command("storybook", "Start a storybook dev server")
  .command("test", "Run tests").argv;

if (argv._.includes("build")) {
  process.env["NODE_ENV"] = "production";
  console.log("[ui-builder]", "creating app bundle");

  const webpackConfig = require("./webpack.config");
  webpack(webpackConfig, (err, stats) => {
    if (err || stats.hasErrors()) {
      // Handle errors here
      console.log("[ui-builder]", "errors during compilation");
      console.log(stats.toString());
    } else {
      console.log("[ui-builder]", "Done!");
    }
  });
}

if (argv._.includes("start")) {
  process.env["NODE_ENV"] = "development";
  const config = require("./config/merged.config");

  const webpackConfig = require("./webpack.config");
  const server = new WebpackDevServer(webpack(webpackConfig), {
    ...webpackConfig.devServer,
  });

  const port = config.port;
  server.startCallback(port, "0.0.0.0", (err) => {
    if (err) {
      console.log("[ui-builder]", "error during dev server startup");
      console.log(err);
    } else {
      console.log("[ui-builder]", `Server started at port ${port}`);
    }
  });
}

if (argv._.includes("test")) {
  process.env["NODE_ENV"] = "development";

  run([], path.resolve(__dirname))
    .then((res) => {
      if (res && res.results) {
        console.log(res.results);
        console.log("[ui-builder]", "Finished running tests");
      }
    })
    .catch((err) => {
      console.log("[ui-builder]", "Error running test");
      console.log(err);
    });
}

if (argv._.includes("init")) {
  const exec = require("./utils/execShellCommand");
  const { parse, assign, stringify } = require("comment-json");

  console.log("[ui-builder]", "Initializing application");
  console.log("[ui-builder]", "copying files...");

  // copy tsconfig.json
  fs.copyFileSync(
    path.resolve(__dirname, "templates/tsconfig.json"),
    path.resolve(env.projectDir, "tsconfig.json")
  );
  console.log("[ui-builder]", "created tsconfig.json");

  // copy default config
  fs.copyFileSync(
    path.resolve(__dirname, "templates/builder.config.js"),
    path.resolve(env.projectDir, "builder.config.js")
  );
  console.log("[ui-builder]", "created builder.config.js");

  // copy css declaration file
  fs.mkdirSync(path.resolve(env.projectDir, "typings"));
  fs.copyFileSync(
    path.resolve(__dirname, "templates/css.d.ts"),
    path.resolve(env.projectDir, "typings/css.d.ts")
  );
  console.log("[ui-builder]", "created css types declaration");

  // create initials src folder
  fs.mkdirSync(path.resolve(env.projectDir, "src"));
  fs.copyFileSync(
    path.resolve(__dirname, "templates/index.tsx"),
    path.resolve(env.projectDir, "src/index.tsx")
  );
  console.log("[ui-builder]", "created src entry point");

  console.log("[ui-builder]", "installing dependencies");

  exec("npm install react react-dom")
    .then(() => {
      return exec(
        "npm install @types/react @types/react-dom @types/node-sass @types/jest -D"
      );
    })
    .then(() => {
      const packageJson = parse(
        fs.readFileSync(path.resolve(env.projectDir, "package.json")).toString()
      );
      const modifiedPackageJson = assign(packageJson, {
        scripts: {
          start: "ui-builder start",
          build: "ui-builder build",
          test: "ui-builder test",
        },
      });
      fs.writeFileSync(
        path.resolve(env.projectDir, "package.json"),
        stringify(modifiedPackageJson, null, 2)
      );
    })
    .then(() => {
      console.log("[ui-builder]", "Done!");
    });
}

if (argv._.includes("storybook")) {
  console.log("[ui-builder]", "Starting storybook");

  const storybook = require("./commands/storybook");

  storybook();
}
