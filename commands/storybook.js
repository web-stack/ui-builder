const path = require("path");
const storybook = require("@storybook/react/standalone");

console.log(path.resolve(__dirname, "../.storybook"));

function main() {
  storybook({
    mode: "dev",
    configDir: path.resolve(__dirname, "../.storybook"),
  });
}

module.exports = main;
