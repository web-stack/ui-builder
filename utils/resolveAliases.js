const { resolve } = require("path");
const { parse } = require("comment-json");
const fs = require("fs");
const env = require("./env");

/**
 * Resolve tsconfig.json paths to Webpack aliases
 * @param  {string} tsconfigPath           - Path to tsconfig
 * @param  {string} webpackConfigBasePath  - Path from tsconfig to Webpack config to create absolute aliases
 * @return {object}                        - Webpack alias config
 */
function resolveWebpackAliases({
  tsconfigPath = resolve(env.projectDir, "tsconfig.json"),
  webpackConfigBasePath = env.projectDir,
} = {}) {
  const { paths } = parse(
    fs.readFileSync(tsconfigPath).toString()
  ).compilerOptions;

  const aliases = {};

  Object.keys(paths).forEach((item) => {
    const key = item.replace("/*", "");
    const value = resolve(
      webpackConfigBasePath,
      paths[item][0].replace("/*", "").replace("*", "")
    );

    aliases[key] = value;
  });

  return aliases;
}

/**
 * Resolve tsconfig.json paths to Webpack aliases
 * @param  {string} tsconfigPath           - Path to tsconfig
 * @param  {string} webpackConfigBasePath  - Path from tsconfig to Webpack config to create absolute aliases
 * @return {object}                        - Webpack alias config
 */
function resolveJestAliases({
  tsconfigPath = resolve(env.projectDir, "tsconfig.json"),
  webpackConfigBasePath = env.projectDir,
} = {}) {
  const { paths } = parse(
    fs.readFileSync(tsconfigPath).toString()
  ).compilerOptions;

  const aliases = {};

  Object.keys(paths).forEach((item) => {
    const key = item.replace("/*", "(.*)$");
    const value = resolve(
      webpackConfigBasePath,
      paths[item][0].replace("/*", "$1").replace("*", "")
    );

    aliases[key] = value;
  });

  return aliases;
}

module.exports = {
  resolveWebpackAliases,
  resolveJestAliases,
};
