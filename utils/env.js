module.exports = {
  isProduction: () => process.env.NODE_ENV === "production",
  projectDir: process.cwd(),
};
