const env = require("./env");

let userConfig;
try {
  userConfig = require(`${env.projectDir}/builder.config.js`);
} catch (e) {
  console.log("Error reading config, using default");
  console.error(e);
}

module.exports = userConfig || {};
