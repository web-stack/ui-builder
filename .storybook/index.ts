const knobs = require("@storybook/addon-knobs");

module.exports = {
  addons: {
    knobs,
  },
};
