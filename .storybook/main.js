const path = require("path");
const env = require("../utils/env");
const webpackConfig = require("../webpack.config");
const config = require("../utils/userConfig");

const stories = path
  .resolve(
    env.projectDir,
    `${config.storybook.componentsDir}\\**\\*.stories.tsx`
  )
  .replace(/\\/g, "/");

module.exports = {
  addons: ["@storybook/addon-knobs/register", "@storybook/addon-docs"],
  stories: [stories],
  webpackFinal: (config) => {
    return {
      ...config,
      module: {
        ...config.module,
        rules: [
          {
            test: /\.(ts|tsx)$/,
            use: [
              "babel-loader",
              require.resolve("react-docgen-typescript-loader"),
            ],
          },
          ...webpackConfig.module.rules,
        ],
      },
      resolve: {
        ...config.resolve,
        extensions: [
          ...config.resolve.extensions,
          ...webpackConfig.resolve.extensions,
        ],
        alias: {
          ...webpackConfig.resolve.alias,
          ...config.resolve.alias,
        },
      },
    };
  },
};
