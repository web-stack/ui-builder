import React from "react";
import ReactDOM from "react-dom";

const root = document.getElementById("root");

ReactDOM.render(
  <main>
    <h1>App is ready to go!</h1>
  </main>,
  root
);
